//
//  CustomView.m
//  DrawingDemo
//
//  Created by James Cash on 10-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "CustomView.h"

@interface CustomView ()

@property (nonatomic,assign) BOOL isTouching;

@end

@implementation CustomView

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(ctx, [[UIColor cyanColor] CGColor]);
    CGContextSetLineWidth(ctx, 1);
    CGContextSetStrokeColorWithColor(ctx, [[UIColor yellowColor] CGColor]);
    CGContextFillRect(ctx, self.bounds);

    CGContextSetFillColorWithColor(ctx, [[UIColor clearColor] CGColor]);
    CGContextSetLineWidth(ctx, self.ringWidth ? self.ringWidth : 10.0);
    CGColorRef ringColor;
    if (self.isTouching) {
        ringColor = [[UIColor blackColor] CGColor];
    } else {
     ringColor = self.ringColor ? self.ringColor.CGColor : [[UIColor purpleColor] CGColor];
    }
    CGContextSetStrokeColorWithColor(ctx, ringColor);
    CGContextStrokeEllipseInRect(ctx, CGRectInset(self.bounds, 15, 15));
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.isTouching = YES;
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.isTouching = NO;
    [self setNeedsDisplay];
}

@end
