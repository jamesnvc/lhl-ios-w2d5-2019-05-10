//
//  ViewController.m
//  DrawingDemo
//
//  Created by James Cash on 10-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"
@import WebKit;
@import SafariServices;

@interface ViewController () <UITextFieldDelegate, WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    CustomView *view = [[CustomView alloc] initWithFrame:CGRectZero];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:view];

    [NSLayoutConstraint activateConstraints:
     @[[view.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],
       [view.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor],
       [view.widthAnchor constraintEqualToAnchor:self.view.widthAnchor multiplier:0.5],
       [view.heightAnchor constraintEqualToAnchor:view.widthAnchor]
       ]];

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    self.textLabel.text = self.textField.text;
    [self.textField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([newString rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]].location != NSNotFound) {
        return NO;
    }
    self.textLabel.text = newString;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.textLabel.text = [self.textLabel.text uppercaseString];
    [textField resignFirstResponder];
    NSURL *url = [NSURL URLWithString:textField.text];
    if (YES) {
        WKWebView *webView =
        [[WKWebView alloc]
         initWithFrame:CGRectMake(0, CGRectGetMidY(self.view.frame),
                                  CGRectGetWidth(self.view.frame),
                                  CGRectGetHeight(self.view.frame) / 2.0)];
        [self.view addSubview:webView];
        webView.navigationDelegate = self;
        [webView loadRequest:[NSURLRequest requestWithURL:url]];
    } else {
        SFSafariViewController *safariVc = [[SFSafariViewController alloc] initWithURL:url];
        [self presentViewController:safariVc animated:YES completion:^{

        }];
    }
    return YES;
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    NSLog(@"went to page");
    [webView evaluateJavaScript:@"document.body.style.backgroundColor = 'red';" completionHandler:^(id _Nullable res, NSError * _Nullable error) {
        NSLog(@"Ran code");
    }];
}

@end
