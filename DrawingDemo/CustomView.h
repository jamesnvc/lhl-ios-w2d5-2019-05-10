//
//  CustomView.h
//  DrawingDemo
//
//  Created by James Cash on 10-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE @interface CustomView : UIView

@property (nonatomic,strong) IBInspectable UIColor *ringColor;
@property (nonatomic,assign) IBInspectable CGFloat ringWidth;

@end

NS_ASSUME_NONNULL_END
