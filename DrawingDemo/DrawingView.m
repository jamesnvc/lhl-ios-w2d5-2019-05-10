//
//  DrawingView.m
//  DrawingDemo
//
//  Created by James Cash on 10-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "DrawingView.h"

@interface DrawingView ()

@property (nonatomic,strong) UIBezierPath *path;

@end

@implementation DrawingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupGestureRecognizer];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupGestureRecognizer];
    }
    return self;
}

- (void)setupGestureRecognizer {
    UILongPressGestureRecognizer * recog = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addPoint:)];
    [self addGestureRecognizer:recog];
}

- (void)addPoint:(UILongPressGestureRecognizer*)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            if (!self.path) {
                self.path = [[UIBezierPath alloc] init];
                [self.path moveToPoint:[sender locationInView:self]];
            } else {
                [self.path addLineToPoint:[sender locationInView:self]];
            }
            NSLog(@"drawing point");
            break;
        case UIGestureRecognizerStateEnded:
            NSLog(@"state ended");
            [self setNeedsDisplay];
            break;
        default:
            return;
    }
}

- (void)drawRect:(CGRect)rect {
    NSLog(@"drawing");
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [[UIColor blackColor] CGColor]);
    [self.path setLineWidth:10];
    [self.path stroke];
}

@end
